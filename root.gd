tool

extends Spatial

export(float) var _unit:float = 1
export(float) var _normal:float = 1
export(int) var _step:int = 100
export(int) var _start:int = 0
export(int) var _stop:int = -1
export(bool) var _init_pose:bool = true
export(bool) var _load_poses:bool = false setget load_poses
export(bool) var _purge:bool = false setget purge

var pose_axis:Spatial = null
var init_transform:Transform = Transform.IDENTITY
var path_mat:SpatialMaterial = null
var normal_mat:SpatialMaterial = null

func purge(b:bool) -> void:
	_purge = false
	if b:
		while $poses.get_child_count() > 0:
			$poses.remove_child($poses.get_child(0))
		pose_axis = null
		init_transform = Transform.IDENTITY

func load_poses(b:bool) -> void:
	_load_poses = false
	if b:
		var f:File = File.new()
		var data = null
		if f.open( "res://metadata.json", File.READ ) == OK:
			var res:JSONParseResult = JSON.parse( f.get_as_text() )
			if res.error == OK:
				data = res.result
			else:
				print( "parsing failed" )
				return
		else:
			print( "loading failed" )
			return
		
		purge(true)
		
		pose_axis = Spatial.new()
		$poses.add_child(pose_axis)
		pose_axis.name = "all_poses"
		pose_axis.owner = $poses.owner
		
		if _init_pose:
			var qp:Dictionary = parse_pose( data.initPose )
			var init:Spatial = $axis.duplicate()
			$poses.add_child(init)
			init.owner = pose_axis.owner
			init.global_transform = Transform( Basis( qp.q ).scaled(Vector3.ONE * 4), qp.p )
			init.visible = true
			init.name = "init"
			init_transform = Transform( Basis(qp.q), qp.p )
		
		var normals:Array = []
		var verts:Array = []
		
		_start = max(0,_start)
		var last:int = _stop
		if last == -1:
			last = data.poses.size()
		else:
			last = min( data.poses.size(), last )
		for i in range( _start+1, last ):
			if i % _step == 0:
				var tmp:Dictionary = parse_pose( data.poses[i] )
				load_pose( tmp )
				verts.append( tmp.p )
				normals.append( Basis( tmp.q ).z )
		
		# tracing the trajectory
		if path_mat == null:
			path_mat = SpatialMaterial.new()
			path_mat.flags_unshaded = true
			path_mat.albedo_color = Color(1,1,1,1)
		if normal_mat == null:
			normal_mat = SpatialMaterial.new()
			normal_mat.flags_unshaded = true
			normal_mat.albedo_color = Color(0,1,1,1)
		
		var imm:ImmediateGeometry = ImmediateGeometry.new()
		imm.begin(Mesh.PRIMITIVE_LINE_STRIP)
		for v in verts:
			imm.add_vertex( v )
		imm.end()
		imm.material_override = path_mat
		$poses.add_child(imm)
		imm.name = "path"
		imm.owner = $poses.owner
		
		imm = ImmediateGeometry.new()
		imm.begin(Mesh.PRIMITIVE_LINES)
		for i in range(0,verts.size()):
			imm.add_vertex( verts[i] )
			imm.add_vertex( verts[i] + normals[i] * _normal * -1 )
		imm.end()
		imm.material_override = normal_mat
		$poses.add_child(imm)
		imm.name = "normals"
		imm.owner = $poses.owner

func parse_pose( qp:Array ) -> Dictionary:
	var t:Transform = Transform( 
		Basis(Quat( qp[0], qp[1], qp[2], qp[3] )).orthonormalized(), 
		Vector3( qp[4], qp[5], qp[6] ) * _unit )
	t = init_transform * t
	return { 
		'q': t.basis.get_rotation_quat(),
		'p': t.origin
		 }

func load_pose( dic:Dictionary ) -> void:
	var pose:Spatial = $axis.duplicate()
	pose_axis.add_child(pose)
	pose.owner = pose_axis.owner
	pose.transform = Transform( Basis( dic.q ), dic.p )
	pose.visible = true

func _ready():
	pass
func _process(delta):
	pass
